import type { ConfigEnv, UserConfig } from 'vite';
import { createViteResolve } from './build/vite/resolve';
import { createVitePlugins } from './build/vite/plugins';
import { createViteBuild } from './build/vite/build';
import { createViteServer } from './build/vite/server';

// https://vitejs.dev/config/
export default (configEnv: ConfigEnv): UserConfig => {
  const { command } = configEnv;

  const isBuild = command === 'build';

  return {
    resolve: createViteResolve(__dirname),
    plugins: createVitePlugins(isBuild, configEnv),
    build: createViteBuild(),
    server: createViteServer(),
  };
};
