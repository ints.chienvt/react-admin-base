import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

export interface AsyncRouteType {
  path: string;
  id: string;
  children: AsyncRouteType[];
}

interface RouteState {
  asyncRouter: AsyncRouteType[];
}

const initialState: RouteState = {
  asyncRouter: [],
};

export const routeSlice = createSlice({
  name: 'route',
  initialState,
  reducers: {
    setStoreAsyncRouter: (state, action: PayloadAction<AsyncRouteType[]>) => {
      state.asyncRouter = action.payload;
    },
  },
});

export const { setStoreAsyncRouter } = routeSlice.actions;

export default routeSlice.reducer;
