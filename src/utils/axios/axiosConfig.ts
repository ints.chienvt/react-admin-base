import type { AxiosRequestConfig, AxiosResponse, InternalAxiosRequestConfig } from 'axios';
import type { RequestOptions, Result } from '#/axios';

export interface CreateAxiosOptions extends AxiosRequestConfig {
  requestOptions?: RequestOptions;
  interceptor?: AxiosInterceptor;
}

type RequestInterceptorsConfig = Pick<CreateAxiosOptions, 'requestOptions' | 'interceptor'> &
  InternalAxiosRequestConfig;

export abstract class AxiosInterceptor {
  beforeRequestHook?: (config: AxiosRequestConfig, options: RequestOptions) => AxiosRequestConfig;
  requestHook?: (res: AxiosResponse<Result>, options: RequestOptions) => any;
  requestCatchHook?: (e: Error, options: RequestOptions) => Promise<any>;
  requestInterceptors?: (config: RequestInterceptorsConfig) => RequestInterceptorsConfig;
  requestInterceptorsCatch?: (error: Error) => void;
  responseInterceptors?: (res: AxiosResponse<any>) => AxiosResponse<any>;
  responseInterceptorsCatch?: (error: Error) => void;
}
