import CryptoJS from 'crypto-js';
import type { StorageConfig, StorageType, StorageValue } from './types';

const SECRET_KEY = CryptoJS.enc.Utf8.parse('3333e6e143439161');
const SECRET_IV = CryptoJS.enc.Utf8.parse('e3bbe7e3ba84431a');

let config: StorageConfig = {
  prefix: 'xiaosiAdmin',
  expire: 0,
  isEncrypt: false,
};

export const setStorageConfig = (info: StorageConfig) => {
  config = { ...config, ...info };
};

export const isSupportStorage = () => {
  return typeof Storage !== 'undefined' ? true : false;
};

export const setStorage = <T>(
  key: string,
  value: StorageValue<T>,
  expire = 0,
  type: StorageType = 'localStorage',
) => {
  if (value === null || value === undefined) {
    value = null;
  }

  if (isNaN(expire) || expire < 0) throw new Error('Expire 必须是数字');

  if (config.expire > 0 || expire > 0) expire = (expire ? expire : config.expire) * 1000;
  const data = {
    value: value,
    time: Date.now(),
    expire: expire,
  };

  const encryptString = config.isEncrypt ? encrypt(JSON.stringify(data)) : JSON.stringify(data);

  window[type].setItem(autoAddPrefix(key), encryptString);
};

export const getStorage = <T>(key: string, type: StorageType = 'localStorage'): StorageValue<T> => {
  key = autoAddPrefix(key);
  if (!window[type].getItem(key) || JSON.stringify(window[type].getItem(key)) === 'null') {
    return null;
  }

  const storage = config.isEncrypt
    ? JSON.parse(decrypt(window[type].getItem(key) || ''))
    : JSON.parse(window[type].getItem(key) || '');

  const nowTime = Date.now();

  if (storage.expire && config.expire * 6000 < nowTime - storage.time) {
    removeStorage(key);
    return null;
  } else {
    setStorage(autoRemovePrefix(key), storage.value);
    return storage.value;
  }
};

export const hasStorage = (key: string): boolean => {
  key = autoAddPrefix(key);
  const arr = getStorageAll().filter((item) => {
    return item.key === key;
  });
  return arr.length ? true : false;
};

export const getStorageKeys = (): (string | null)[] => {
  const items = getStorageAll();
  const keys = [];
  for (let index = 0; index < items.length; index++) {
    keys.push(items[index].key);
  }
  return keys;
};

export const getStorageForIndex = (index: number, type: StorageType = 'localStorage') => {
  return window[type].key(index);
};

export const getStorageLength = (type: StorageType = 'localStorage') => {
  return window[type].length;
};

export const getStorageAll = (type: StorageType = 'localStorage') => {
  const len = window[type].length;
  const arr = [];
  for (let i = 0; i < len; i++) {
    const getKey = window[type].key(i) || '';
    const getVal = window[type].getItem(getKey);
    arr[i] = { key: getKey, val: getVal };
  }
  return arr;
};

export const removeStorage = (key: string, type: StorageType = 'localStorage') => {
  window[type].removeItem(autoAddPrefix(key));
};

export const clearStorage = (type: StorageType = 'localStorage') => {
  window[type].clear();
};

const autoAddPrefix = (key: string): string => {
  const prefix = config.prefix ? config.prefix + '_' : '';
  return prefix + key;
};

const autoRemovePrefix = (key: string) => {
  const len = config.prefix ? config.prefix.length + 1 : 0;
  return key.substr(len);
};

/** SessionStorage */

export const setSessionStorage = <T>(key: string, value: StorageValue<T>, expire = 0) => {
  return setStorage<T>(key, value, expire, 'sessionStorage');
};

export const getSessionStorage = <T>(key: string): StorageValue<T> => {
  return getStorage<T>(key, 'sessionStorage');
};

export const getSessionStorageForIndex = (index: number) => {
  return getStorageForIndex(index, 'sessionStorage');
};

export const getSessionStorageLength = () => {
  return getStorageLength('sessionStorage');
};

export const getSessionStorageAll = () => {
  return getStorageAll('sessionStorage');
};

export const removeSessionStorage = (key: string) => {
  return removeStorage(key, 'sessionStorage');
};

export const clearSessionStorage = () => {
  return clearStorage('sessionStorage');
};

/**
 * @param data
 * @returns {string}
 */
const encrypt = (data: string): string => {
  if (typeof data === 'object') {
    try {
      data = JSON.stringify(data);
    } catch (error) {
      console.error('encrypt error:', error);
    }
  }
  const dataHex = CryptoJS.enc.Utf8.parse(data);
  const encrypted = CryptoJS.AES.encrypt(dataHex, SECRET_KEY, {
    iv: SECRET_IV,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.ciphertext.toString();
};

/**
 * @param data
 * @returns {string}
 */
const decrypt = (data: string): string => {
  const encryptedHexStr = CryptoJS.enc.Hex.parse(data);
  const str = CryptoJS.enc.Base64.stringify(encryptedHexStr);
  const decrypt = CryptoJS.AES.decrypt(str, SECRET_KEY, {
    iv: SECRET_IV,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  const decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return decryptedStr.toString();
};
