import { Card, Col, Row } from 'antd';
import { memo } from 'react';
import './index.less';

const Home = memo(() => {
  return (
    <div className="">
      <Row gutter={[12, 12]}>
        <Col lg={18} sm={24} xs={24}>
          <Card size="small" title="Home 1">
            Home 1
          </Card>
        </Col>
        <Col lg={6} sm={24} xs={24}>
          <Card size="small" title="Home 2">
            Home 2
          </Card>
        </Col>
        <Col lg={18} sm={24} xs={24}>
          <Card size="small" title="Home 3">
            Home 3
          </Card>
        </Col>
        <Col lg={6} sm={24} xs={24}>
          <Card size="small" title="Home 4">
            Home 4
          </Card>
        </Col>
      </Row>
    </div>
  );
});

export default Home;
