## Characteristic

- **The latest technology stack**: Developed using front-end cutting-edge technologies such as React18/Vite4

- **Theme**: configurable theme

- **Internationalization**: built-in perfect internationalization scheme

- **Permission**: Permission Routing

## 1.Prepare

- [Node](http://nodejs.org/) And [Git](https://git-scm.com/) - Project development environment
- [Vite](https://cn.vitejs.dev/) - Familiar with Vite features
- [React18](https://reactjs.org/) - Familiar with the basic syntax of React18
- [Es6+](http://es6.ruanyifeng.com/) - familiar with the basic syntax of Es6
- [React Router V6](https://reactrouter.com/en/main) - Familiar with the basic use of React Router V6
- [And Design 5](https://ant.design/docs/react/introduce-cn) - Ui basic use
- [Emotion](https://emotion.sh/docs/introduction) - Basic use

### 2.Installation Dependencies

Recommended`pnpm`

```bash
pnpm i
```

`npm` install

```bash
pnpm install
```

### 3.Developer

```bash
pnpm run dev
```

### 4.Production

```bash
pnpm run build
```

## How to contribute

**Pull Request:**

1. Fork Code!
2. Create your own branch: `git checkout -b feature/xxxx`
3. Submit your changes: `git commit -m 'feature: add xxxxx'`
4. Push your branch: `git push origin feature/xxxx`
5. Submit: `pull request`

  - `feat` New Features
  - `fix` Repair defects
  - `docs` Document change
  - `style` Code format
  - `refactor` Code refactoring
  - `perf` Performance optimization
  - `test` Add neglected tests or changes to existing tests
  - `build` Build processes, external dependency changes (such as upgrading npm packages, modifying packaging configurations, etc.)
  - `ci` Modify CI configuration and scripts
  - `revert` Roll back the commit
  - `chore` Changes to the build process or tools and libraries (do not affect source files)
  - `wip` Under development
  - `types` Type definition file modification