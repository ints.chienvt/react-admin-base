import { defineFakeRoute } from 'vite-plugin-fake-server/client';

const userInfo = {
  name: 'Admin',
  userid: '00000001',
  email: '1531733886@gmail.com',
  signature: 'Admin',
  introduction: 'Admin',
  title: 'Admin',
  token: '',
  power: 'admin',
};

const userInfo2 = {
  name: 'Test',
  userid: '00000002',
  email: '12312311223@gmail.com',
  signature: 'Test',
  introduction: 'Test',
  title: 'Test',
  token: '',
  power: 'test',
};

export default defineFakeRoute([
  {
    url: '/mock_api/login',
    timeout: 1000,
    method: 'post',
    response: ({ body }: { body: Recordable }) => {
      const { username, password } = body;
      if (username == 'admin' && password == 'admin123') {
        userInfo.token = genID(16);
        return {
          data: userInfo,
          code: 1,
          message: 'ok',
        };
      } else if (username == 'test' && password == 'test123') {
        userInfo2.token = genID(16);
        return {
          data: userInfo2,
          code: 1,
          message: 'ok',
        };
      } else {
        return {
          data: null,
          code: -1,
          message: 'Error',
        };
      }
    },
  },
  {
    url: '/mock_api/getUserInfo',
    timeout: 1000,
    method: 'get',
    response: () => {
      return userInfo;
    },
  },
]);

function genID(length: number) {
  return Number(Math.random().toString().substr(3, length) + Date.now()).toString(36);
}
