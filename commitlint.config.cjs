/* eslint-disable @typescript-eslint/no-var-requires */
// CommonJS
const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');

const scopes = fs.readdirSync(path.resolve(__dirname, 'src')).map((i) => i.toLowerCase());

const gitStatus = execSync('git status --porcelain || true').toString().trim().split('\n');

const scopeComplete = gitStatus
  .find((r) => ~r.indexOf('M  src'))
  ?.replace(/(\/)/g, '%%')
  ?.match(/src%%((\w|-)*)/)?.[1];

const subjectComplete = gitStatus
  .find((r) => ~r.indexOf('M  src'))
  ?.replace(/\//g, '%%')
  ?.match(/src%%((\w|-)*)/)?.[1];

module.exports = {
  extends: ['@jsxiaosi/commitlint-config'],
  prompt: {
    scopes: [...scopes, 'mock'],
    enableMultipleScopes: true,
    scopeEnumSeparator: ',',
    customScopesAlign: !scopeComplete ? 'top' : 'bottom',
    defaultScope: scopeComplete,
    defaultSubject: subjectComplete && `[${subjectComplete}] `,
  },
};
