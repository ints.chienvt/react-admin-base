import type { BuildOptions } from 'vite';

export function createViteBuild(): BuildOptions {
  const viteBuild = {
    target: 'es2015',
    outDir: 'dist',
    cssTarget: 'chrome80',

    assetsDir: 'static',
    cssCodeSplit: true,
    sourcemap: false,
    brotliSize: false,
    // minify: 'terser',
    // terserOptions: {
    //   compress: {
    //     drop_console: true,
    //   },
    // },
    chunkSizeWarningLimit: 2000,
  };
  return viteBuild;
}
