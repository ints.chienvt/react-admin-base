import path from 'path';
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons';

export function configSvgPlugin() {
  const svgPlugin = createSvgIconsPlugin({
    iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
    symbolId: 'icon-[dir]-[name]',
  });
  return svgPlugin;
}
