import type { ServerOptions } from 'vite';

export function createViteServer(): ServerOptions {
  const viteServer: ServerOptions = {
    host: true,
    port: 3000,
    strictPort: false,
    // boolean | string
    // open: true,
    // boolean | CorsOptions
    // cors: true,
    // force: false,
    proxy: {
      '/api': {
        target: '',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    },
  };
  return viteServer;
}
